import { Body, Controller, Post ,HttpCode, HttpStatus } from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { User } from 'src/auth/models/user.interface';
import { AuthService } from 'src/auth/services/auth/auth.service';
import { map } from 'rxjs/operators';
@Controller('auth')
export class AuthController {
    constructor(private authService : AuthService){}

    
    
    @Post('register')
    register(@Body() user:User):Observable<User>{
        return from(this.authService.registerAccount(user))
    }


    @Post('login')
  @HttpCode(HttpStatus.OK)
  login(@Body() user: User): Observable<{ token: string }> {
    let connected = this.authService.findUser(user.email)
    return this.authService
      .login(user)
      .pipe(map((jwt: string) => ({ token: jwt , connected:user.email })));
  }
}
