import { Module } from '@nestjs/common';
import { AuthService } from './services/auth/auth.service';
import { AuthController } from './controllers/auth/auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './models/user.entity';
import { JwtModule } from '@nestjs/jwt';
import { JwtGuard } from './guard/jwt.guard';
import { JwtStrategy } from './guard/jwt.strategy';
import { RolesGuard } from './guard/roles.guard';

@Module({
  imports:[JwtModule.registerAsync({
    useFactory: () =>({
      secret:process.env.JWT_SECRET,
      signOptions : {expiresIn:'600000s'}
    })
  }),
    TypeOrmModule.forFeature([UserEntity])
  ],
  providers: [AuthService , JwtGuard,JwtStrategy,RolesGuard],
  controllers: [AuthController],
  exports:[AuthService]
})
export class AuthModule {}
