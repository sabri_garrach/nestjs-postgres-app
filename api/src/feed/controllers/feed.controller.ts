import {
  Controller,
  Post,
  Body,
  Get,
  Put,
  Request,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { FeedPost } from '../models/post.interface';
import { FeedService } from '../services/feed.service';
import { find, from, Observable } from 'rxjs';
import { UpdateResult, DeleteResult } from 'typeorm';
import { JwtGuard } from '../../auth/guard/jwt.guard';
import { RolesGuard } from '../../auth/guard/roles.guard';
import { Role } from '../../auth/models/role.enum';
import { Roles } from '../../auth/decorators/roles.decorator';
import { IsCreatorGuard } from '../guard/is-creator.guard';
@Controller('feed')
export class FeedController {
  constructor(private feedService: FeedService) {}
  //ajouter un article
  @Roles(Role.ADMIN,Role.USER, Role.PREMIUM)
  @UseGuards(JwtGuard, RolesGuard)
  @Post()
  create(@Body() feedPost: FeedPost, @Request() req): Observable<FeedPost> {
    return this.feedService.createPost(req.user, feedPost);
  }
  //trouver tous les articles
  @Get()
  findAll(): Observable<FeedPost[]> {
    return this.feedService.findAllPosts();
  }
  //trouver un article par ID
  @Get(':id')
  find(@Param('id') id: number): Observable<FeedPost> {
    return this.feedService.findByIdPost(id);
  }
  //modifier un article
  @UseGuards(JwtGuard, IsCreatorGuard)
  @Put(':id')
  update(
    @Param('id') id: number,
    @Body() feedPost: FeedPost,
  ): Observable<UpdateResult> {
    return this.feedService.updatePost(id, feedPost);
  }

  //supprimer un article
  @UseGuards(JwtGuard, IsCreatorGuard)
  @Delete(':id')
  delete(@Param('id') id: number): Observable<DeleteResult> {
    return this.feedService.deletePost(id);
  }
}
