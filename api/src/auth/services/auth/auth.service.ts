import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';

import * as bcrypt from 'bcrypt';
import { from, Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Repository } from 'typeorm';
import { UserEntity } from 'src/auth/models/user.entity';
import { User } from 'src/auth/models/user.interface';
@Injectable()
export class AuthService {

    constructor(@InjectRepository(UserEntity) 
    
    private readonly userRepository : Repository<UserEntity>,
    private jwtService: JwtService
    ){}
    hashPassword(password: string ):Observable<string>{
       return from(bcrypt.hash(password,12))

    }

    registerAccount(user:User):Observable<User>{
        const{firstName,lastName,email,password} = user
        
        return this.hashPassword(password).pipe(
            switchMap((hashedPassword: string)=>{
                return from(this.userRepository.save({
                    firstName,
                    lastName,
                    email,
                    password:hashedPassword
                }),
                ).pipe(
                    map((user: User)=> {
                    delete user.password;
                    return user;
                  }),
                  );

            }),
        );
    }




    validateUser(email: string, password: string): Observable<User> {
        return from(
          this.userRepository.findOne(
            { email },
            {
              select: ['id', 'firstName', 'lastName', 'email', 'password', 'role'],
            },
          ),
        ).pipe(
          switchMap((user: User) => {
            if (!user) {
              throw new HttpException(
                { status: HttpStatus.FORBIDDEN, error: 'Invalid Credentials' },
                HttpStatus.FORBIDDEN,
              );
            }
            return from(bcrypt.compare(password, user.password)).pipe(
              map((isValidPassword: boolean) => {
                if (isValidPassword) {
                  delete user.password;
                  return user;
                }
              }),
            );
          }),
        );
      }
    

      login(user: User): Observable<string> {
        const { email, password } = user;
        return this.validateUser(email, password).pipe(
          switchMap((user: User) => {
            if (user) {
              // create JWT - credentials
              return from(this.jwtService.signAsync({ user }));
            }
          }),
        );
      }

      findUserById(id: number ):Observable<User>{
        return from (
          this.userRepository.findOne({id},{relations : ['feedPosts']})
        
        ).pipe(
          map((user:User) =>{
            delete user.password
            return user 
          })
        )
      }
      findUser(email: string ):Observable<User>{
        return from (
          this.userRepository.findOne({email},{relations : ['feedPosts']})
          )
      }
    
      getJwtUser(jwt: string): Observable<User | null> {
        return from(this.jwtService.verifyAsync(jwt)).pipe(
          map(({ user }: { user: User }) => {
            return user;
          }),
          catchError(() => {
            return of(null);
          }),
        );
      }
    
}
